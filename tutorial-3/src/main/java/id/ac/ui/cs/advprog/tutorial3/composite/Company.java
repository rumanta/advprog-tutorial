package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        int totalSalary = 0;

        for (Employees emp : this.employeesList) {
            totalSalary += emp.getSalary();
        }

        return totalSalary;
    }

    public List<Employees> getAllEmployees() {
        return this.employeesList;
    }
}
