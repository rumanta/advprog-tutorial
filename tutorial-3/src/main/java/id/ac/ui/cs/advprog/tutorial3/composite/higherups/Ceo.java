package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "CEO";
        if (salary < 200000) {
            throw new IllegalArgumentException("CEO Salary must not lower than 200000.00");
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
