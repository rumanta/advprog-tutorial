package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "Back End Programmer";
        if (salary < 20000) {
            throw new IllegalArgumentException("BackendProgrammer Salary must not lower than 20000.00");
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
