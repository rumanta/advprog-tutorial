package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class StinkyClams implements Clams {

    public String toString() {
        return "Stinky Clams from leftovers";
    }
}
