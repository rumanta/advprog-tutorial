package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class TossedCrustDough implements Dough {
    public String toString() {
        return "Tossed Crust style soft crust dough";
    }
}
